//
//  PlanetData.swift
//  Alien Adventure
//
//  Created by Jarrod Parkes on 10/4/15.
//  Copyright © 2015 Udacity. All rights reserved.
//
import Foundation

extension Hero {
    
    func planetData(dataFile: String) -> String {
        
        var totalPoints = Int()
        var planet = String()
        
        enum pointSystem: Int {
            case Common = 1
            case Uncommon = 2
            case Rare = 3
            case Legendary = 4
        }
        
        let dataFileURL = NSBundle.mainBundle().URLForResource(dataFile, withExtension: "json")!
        
        let data = NSData(contentsOfURL: dataFileURL)
        
        var arrayFromJSON: NSArray!
        
        do {
            arrayFromJSON = try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions()) as! NSArray
        }
        
        for item in arrayFromJSON {
            var points = 0
            
            if let item = item as? NSDictionary {
                points += item["CommonItemsDetected"] as! Int * pointSystem.Common.rawValue
                points += item["UncommonItemsDetected"] as! Int * pointSystem.Uncommon.rawValue
                points += item["RareItemsDetected"] as! Int * pointSystem.Rare.rawValue
                points += item["LegendaryItemsDetected"] as! Int * pointSystem.Legendary.rawValue
                
                if points > totalPoints {
                    totalPoints = points
                    planet = item["Name"] as! String
                }
                
            }
        }
        
        
        return planet
    }
}

// If you have completed this function and it is working correctly, feel free to skip this part of the adventure by opening the "Under the Hood" folder, and making the following change in Settings.swift: "static var RequestsToSkip = 7"